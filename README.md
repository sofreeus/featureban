<!-- Change the title of the class -->

# Featureban - Kanban Simulation Game

Featureban Simulation Game!

Stop Starting, Start Finishing!

Manage (Improve?) your teams’ throughput and flow!

For dev and ops and all types of teams and work!


Does your team seem to struggle to get work done? Do you feel like you have 83 in progress things assigned to you, all of them only partially finished? Does it seem like you are always starting work, but never finishing?

You are not alone!

In this hands on simulation we’ll explore why work piles up, and what we can do about it. Using a kanban simulation game we’ll start by making work visible, add WIP limits, and look at metrics. While this is a simulation game, there will be concepts and ideas you can take back to your teams and start implementing immediately!

---

### Class Goal

<!-- 4-6 sentences -->
Students will learn how to visualize work, how to implement WIP limits, some basics about the kanban model, and some useful metrics.

---

### Success Metric

<!-- 1-2 sentences -->
You should leave with at least 1 idea you can implement at work on Monday to manage work better.

---

### Prerequisites:

<!-- Update this list to specify all the requirements for your class. -->
<!-- Define your expectations for the baseline student.  -->
<!-- Note: Using https://repl.it can significantly simplify requirements for programming-related classes. -->

- Desire to learn better ways to manage work!


---


# References / Inspiration

This class is based on Featureban by AgendaShift and Mike Burrows. The original Featureban and any variations used by Software Freedom School and Agile Ideation, LLC are licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/.

https://www.agendashift.com/resources/featureban

<!-- OPTIONAL: Create a Table of Contents. -->
<!-- Make sure to keep this up to date as you make edits. -->
<!-- If you opt not to do this, a link to the first lab is helpful. -->

### 0. Introduction

### 1. Visual Management

### 2. WIP Limits

### 3. Kanban model

### 4. Metrics

---

<!-- Feel free to add additional materials -->

<!-- Examples:
     Recommended Reading
     General course notes
     Other links and reference materials
-->

---

### Contributing

Feel free to submit an Issue or open a Merge Request against this repository if you notice any mis-spellings, glaring omissions, or opportunities for improvement.
